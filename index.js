console.log("Hello World");

console.log("Hello Everyone");

// comments

//This is a single comment line

// ctrl + /
// Multiline comments

// shift + alt + /

// Statements in programming are instructions that we tell the computer to program

// Syntax in programming, it is the set rules that describes how statements must be constructed.

// Variables

/* 
 -it is used to contain data
 -Syntax in declaring variables
 -let/const variableName
*/

let myVariable;
myVariable = "Hello";
console.log(myVariable);

// let hello;
/* 
   Guides in writing variables"
   1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
   2. Variables names should start with a lowercaes character, use camelCase for multiple words.
   3. For constant variables, use the 'const' keyword.
   4. Varible names should be indicative (or descriptive) of the value being stored to a avoid confusion.
   5. Never name a variable starting with numbers.
*/

/* 
   Declaring variables with an initial value.
   let/const a variable = value;
*/
let productName = "destkop computer";

console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
console.log(interest);

let priceWithInterest = productPrice * interest;
console.log(Math.floor(priceWithInterest));

let product = "Alvin's computer";
console.log(product);

// follow the syntax.
// let - we usually change/reassign the values in our variable

// Reassuring variable values
// Syntax

// variableName = newValue;

productName = "Laptop";

console.log(productName);

console.log(interest);
// console.log(friend);
// only be declared once.

//declaration
let supplier;

//Initialization is done after the variables have been declared.
supplier = "John Smith Tradings";

console.log(supplier);

//Reassignment of variables because it's initial value was already declared
supplier = "Zuitt Store";
console.log(supplier);

const pi = 3.1416;
//for const must be declared with value;

//3.1416

console.log(pi);

// var vs let/const

// var(es1) vs let/const(es6)
// var can be hoisted.
// Multiple variable declaration

// can be declared for
let productCode = "DC017";
const productBrand = "Dell";

console.log(productCode, productBrand);

// using a variable with a reserved keyword
// console.log(let); //error: cannot use reserved keyword as a variable name.

// [SECTION] Data Types

// String series of characters that creates a word, a phrase, a sentence or anything related to creating text.
// Strings in JavaScript a single ('') or double ("") quote.

let country = "Philippines";
let province = "Metro Manila";

// concatination of strings
let fullAddress = province + "," + country;
console.log(fullAddress);
// multiple string values can be combined to create a single string using the "+" symbol.
let greeting = "I live in the \n" + country;
console.log(greeting);
let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);

let message = "John's employees went home early.";
console.log(message);

message = "John's message went home early";
console.log(message);
// Number
// Integers/Whole Numbers
let headcount = 26;
console.log(headcount);

// Decimal Numbers/ Fractions
let grade = 98.7;
console.log(grade);
// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combine number and strings
console.log("John's grade last quarter is " + grade);

// Boolean
// true/false
let isMarried = false;
let isGoodConduct = true;
console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);

// Objects - Array - Store multiple values with similar data types
// Arrays
// It is used to store multiple values with similar data types
// Syntax:
// let/const arrayName = [elementA, elementB, elementC, ....];
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

let details = ["John", "Smith", 32, true];
console.log(details);
